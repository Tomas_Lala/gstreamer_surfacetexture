package cz.lala.carberry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.lala.carberry.R;

/**
 * Created by tomas on 28.3.16.
 */
public class LaunchActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_gstreamer)
    public void onGsreamerClick(){
        startActivity(new Intent(this,TestGStreamerActivity.class));
    }

    @OnClick(R.id.btn_texture)
    public void onGsreamerTexture(){
        startActivity(new Intent(this,TestCardBoardActivity.class));
    }

    @OnClick(R.id.btn_surface)
    public void onGsreamerSurface(){
        startActivity(new Intent(this,TestSurfaceActivity.class));
    }
}
